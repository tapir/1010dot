
extends Node

var globals = preload("res://globals.gd")

# Blocks
const blocks = {
	"block_dot": {
		"color": Color(0x7f8dd4ff),
		"size" : Vector2(1, 1),
		"tiles": [Vector2(0, 0)]
	},
	
	"block_big_corner_0": {
		"color": Color(0x5abee2ff),
		"size" : Vector2(3, 3),
		"tiles": [Vector2(0, 0), Vector2(0, 1), Vector2(0, 2), Vector2(1, 2), Vector2(2, 2)]
	},
	
	"block_big_corner_90": {
		"color": Color(0x5abee2ff),
		"size" : Vector2(3, 3),
		"tiles": [Vector2(0, 2), Vector2(0, 1), Vector2(0, 0), Vector2(1, 0), Vector2(2, 0)]
	},
	
	"block_big_corner_180": {
		"color": Color(0x5abee2ff),
		"size" : Vector2(3, 3),
		"tiles": [Vector2(0, 0), Vector2(1, 0), Vector2(2, 0), Vector2(2, 1), Vector2(2, 2)]
	},
	
	"block_big_corner_270": {
		"color": Color(0x5abee2ff),
		"size" : Vector2(3, 3),
		"tiles": [Vector2(0, 2), Vector2(1, 2), Vector2(2, 2), Vector2(2, 1), Vector2(2, 0)]
	},
		
	"block_corner_0": {
		"color": Color(0x57cb84ff),
		"size" : Vector2(2, 2),
		"tiles": [Vector2(0, 0), Vector2(0, 1), Vector2(1, 1)]
	},
	
	"block_corner_90": {
		"color": Color(0x57cb84ff),
		"size" : Vector2(2, 2),
		"tiles": [Vector2(0, 1), Vector2(0, 0), Vector2(1, 0)]
	},
	
	"block_corner_180": {
		"color": Color(0x57cb84ff),
		"size" : Vector2(2, 2),
		"tiles": [Vector2(0, 0), Vector2(1, 0), Vector2(1, 1)]
	},
	
	"block_corner_270": {
		"color": Color(0x57cb84ff),
		"size" : Vector2(2, 2),
		"tiles": [Vector2(0, 1), Vector2(1, 1), Vector2(1, 0)]
	},
	
	"block_big_square": {
		"color": Color(0x4cd4b0ff),
		"size" : Vector2(3, 3),
		"tiles": [Vector2(0, 0), Vector2(1, 0), Vector2(2, 0), Vector2(0, 1), Vector2(1, 1), Vector2(2, 1), Vector2(0, 2), Vector2(1, 2), Vector2(2, 2)]
	},
	
	"block_square": {
		"color": Color(0x99db53ff),
		"size" : Vector2(2, 2),
		"tiles": [Vector2(0, 0), Vector2(1, 0), Vector2(0, 1), Vector2(1, 1)]
	},
	
	"block_longest_line_0": {
		"color": Color(0xda6554ff),
		"size" : Vector2(5, 1),
		"tiles": [Vector2(0, 0), Vector2(1, 0), Vector2(2, 0), Vector2(3, 0), Vector2(4, 0)]
	},
	
	"block_longest_line_90": {
		"color": Color(0xda6554ff),
		"size" : Vector2(1, 5),
		"tiles": [Vector2(0, 0), Vector2(0, 1), Vector2(0, 2), Vector2(0, 3), Vector2(0, 4)]
	},
	
	"block_longer_line_0": {
		"color": Color(0xe76a82ff),
		"size" : Vector2(4, 1),
		"tiles": [Vector2(0, 0), Vector2(1, 0), Vector2(2, 0), Vector2(3, 0)]
	},
	
	"block_longer_line_90": {
		"color": Color(0xe76a82ff),
		"size" : Vector2(1, 4),
		"tiles": [Vector2(0, 0), Vector2(0, 1), Vector2(0, 2), Vector2(0, 3)]
	},
	
	"block_long_line_0": {
		"color": Color(0xec9548ff),
		"size" : Vector2(3, 1),
		"tiles": [Vector2(0, 0), Vector2(1, 0), Vector2(2, 0)]
	},
	
	"block_long_line_90": {
		"color": Color(0xec9548ff),
		"size" : Vector2(1, 3),
		"tiles": [Vector2(0, 0), Vector2(0, 1), Vector2(0, 2)]
	},
	
	"block_line_0": {
		"color": Color(0xfec63dff),
		"size" : Vector2(2, 1),
		"tiles": [Vector2(0, 0), Vector2(1, 0)]
	},
	
	"block_long_line_90": {
		"color": Color(0xfec63dff),
		"size" : Vector2(1, 2),
		"tiles": [Vector2(0, 0), Vector2(0, 1)]
	}
}

func create_random_block():
	var block = KinematicBody2D.new()
	var tiles_node = Node2D.new()
	var name = blocks.keys()[randi() % blocks.size()]
	
	for tile in blocks[name]["tiles"]:
		var s = Sprite.new()
		s.set_centered(false)
		s.set_texture(preload("res://tile.png"))
		s.set_pos(tile * globals.total_size)
		s.set_modulate(blocks[name]["color"])
		tiles_node.add_child(s)
	
	block.set_meta("typo", name)
	tiles_node.set_name("tiles")
	block.add_child(tiles_node)
	
	return block

func block_drag_mode(block):
	block.set_scale(Vector2(1, 1))
	for node in block.get_node("tiles").get_children():
		node.set_scale(Vector2(0.9, 0.9))

func block_normal_mode(block):
	block.set_scale(Vector2(1, 1))
	for node in block.get_node("tiles").get_children():
		node.set_scale(Vector2(1, 1))
		
func block_list_mode(block):
	block.set_scale(Vector2(0.5, 0.5))
	for node in block.get_node("tiles").get_children():
		node.set_scale(Vector2(1, 1))