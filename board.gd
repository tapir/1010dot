
extends Node2D

var globals = preload("res://globals.gd")
var bf      = preload("res://block_funcs.gd").new()

# Board and tile settings
const size          = 10
const size_px       = 580
const tile_free     = 0
const tile_occupied = 1
const tile_scene    = preload("res://tile.scn")
const color_empty   = Color(0x444444ff)

# Board state
var board       = {}
var target_tile = Vector2(size, size)
var next_shapes = {}
var dragged_block = null
var pos_list    = Vector2(20, size_px + 20)
var shape_space = size_px / 3
var released    = true
var index
var score       = 0
var return_home = false

# Check game over
func check_game_over():
	if next_shapes.size() != 0:
		for i in next_shapes:
			for j in range(size):
				for k in range(size):
					var tar = Vector2(j, k)
					var b = next_shapes[i]["node"]
					var namo = b.get_meta("typo")
					
					# Check if occupation is legal
					var occupation_legal = true
					for tile in bf.blocks[namo]["tiles"]:
						var pos = tile + tar
						
						if pos.x >= size || pos.y >= size || pos.x < 0 || pos.y < 0:
							occupation_legal = false
							break
						
						if board[pos]["state"] == tile_occupied:
							occupation_legal = false
							break
					
					if occupation_legal:
						return false
		return true
	else:
		return false

# Delete full lines
func delete_vertical_line(line):
	for i in range(10):
		var index = Vector2(line, i)
		board[index]["node"].set_modulate(color_empty)
		board[index]["state"] = tile_free
		
func delete_horizontal_line(line):
	for i in range(10):
		var index = Vector2(i, line)
		board[index]["node"].set_modulate(color_empty)
		board[index]["state"] = tile_free

# Check lines
func check_lines():
	var total_num = 0
	
	# check vert
	for i in range(size):
		var line_full = true
		for j in range(size):
			var index = Vector2(i, j)
			var state = board[index]["state"]
			if state == tile_free:
				line_full = false
				break
		if line_full:
			total_num += 1
			delete_vertical_line(i)
			
	# check horiz
	for i in range(size):
		var line_full = true
		for j in range(size):
			var index = Vector2(j, i)
			var state = board[index]["state"]
			if state == tile_free:
				line_full = false
				break
		if line_full:
			total_num += 1
			delete_horizontal_line(i)
			
	if total_num > 0:
		score += pow(1.5, total_num) * 100

# Reset board
func reset_board():
	for tile in board:
		board[tile]["node"].set_modulate(color_empty)
		board[tile]["state"] = tile_free

# Create empty board
func create_empty_board():
	for i in range(size):
		for j in range(size):
			var new_tile = tile_scene.instance()
			var x = i * globals.total_size
			var y = j * globals.total_size
			new_tile.set_pos(Vector2(x, y))
			get_node("tiles").add_child(new_tile)
			board[Vector2(i, j)] = { "node": new_tile, "state": tile_free }

# Check if dropping the block is legal
func is_move_legal():
	var namo = dragged_block.get_meta("typo")
	# Check if occupation is legal
	for tile in bf.blocks[namo]["tiles"]:
		var pos = tile + target_tile
		
		if pos.x >= size || pos.y >= size || pos.x < 0 || pos.y < 0:
			return false
		
		if board[pos]["state"] == tile_occupied:
			return false
			
	return true
		
# Occupy tiles
func occupy_tiles():
	var type = dragged_block.get_meta("typo")
	
	# Then occupy the tiles		
	for tile in bf.blocks[type]["tiles"]:
		var pos = tile + target_tile
		board[pos]["node"].set_modulate(bf.blocks[type]["color"])
		board[pos]["state"] = tile_occupied
		
	dragged_block = null

# Input
func _input(event):
	if (event.type == InputEvent.MOUSE_MOTION) && dragged_block:
			dragged_block.set_pos(event.pos)
	
	if (event.type == InputEvent.MOUSE_BUTTON) && (event.button_index == BUTTON_RIGHT) && dragged_block:
		return_home = true
	
	if (event.type == InputEvent.MOUSE_BUTTON) && (event.button_index == BUTTON_LEFT):
		if released:
			var m_pos = event.pos
			for i in next_shapes:
				var n = next_shapes[i]["node"].get_pos()
				var s = next_shapes[i]["area_size"] + n
				if event.pos.x > n.x && event.pos.x < s.x && event.pos.y > n.y && event.pos.y < s.y:
					dragged_block = next_shapes[i]["node"]
					bf.block_drag_mode(dragged_block)
					index = i
					released = false
		else:
			if dragged_block:
				if is_move_legal():
					released = true

# Main loop
func _process(delta):
	get_node("Control").get_node("score").set_text(str(score))
	
	# Populate next shapes
	if next_shapes.size() == 0:
		for i in range(3):
			var sh = bf.create_random_block()
			var b_pos = pos_list + Vector2(i * shape_space, 0)
			var namo = sh.get_meta("typo")
			var area_size = bf.blocks[namo]["size"] * globals.total_size * 0.5
			bf.block_list_mode(sh)
			sh.set_pos(b_pos)
			next_shapes[i] = {"node": sh, "area_size": area_size, "home": b_pos}
			add_child(sh)
			get_node("Control").get_node("game_over").set_text(str(check_game_over()))

	if return_home:
		dragged_block = null
		var p = next_shapes[index]["home"]
		var b = next_shapes[index]["node"]
		bf.block_list_mode(b)
		b.move((p - b.get_pos()) * delta * 50)
		if b.get_pos().distance_to(p) < 10:
			b.set_pos(p)
			released = true
			return_home = false

	if dragged_block:
		var block_corner = dragged_block.get_pos()
		
		if released:
			var target_pos = target_tile * globals.total_size + get_node("tiles").get_pos()
			bf.block_normal_mode(dragged_block)
			dragged_block.move((target_pos - block_corner) * delta * 50)
			if block_corner.distance_to(target_pos) < 10:
				occupy_tiles()
				next_shapes[index]["node"].queue_free()
				next_shapes.erase(index)
				dragged_block = null
				check_lines()
				get_node("Control").get_node("game_over").set_text(str(check_game_over()))
		else:
			# Calculate snapping position
			var target_f = block_corner / globals.total_size
			target_tile = target_f.floor()
			var sec = (target_f - target_tile) * globals.total_size
			var half_size = globals.tile_size / 2
			
			if sec.x > half_size && sec.y < half_size:
				target_tile += Vector2(1, 0)
			elif sec.x < half_size && sec.y > half_size:
				target_tile += Vector2(0, 1)
			elif sec.x > half_size && sec.y > half_size:
				target_tile += Vector2(1, 1)
	
func _ready():
	randomize()
	create_empty_board()
	set_process(true)
	set_process_input(true)